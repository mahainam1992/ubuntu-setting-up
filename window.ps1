# Kiểm tra và cài đặt Chocolatey
if (-not (Get-Command choco -ErrorAction SilentlyContinue)) {
    Set-ExecutionPolicy Bypass -Scope Process -Force;
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072;
    iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

# Cài đặt các công cụ cần thiết thông qua Chocolatey
choco install vscode -y
choco install eclipse -y
choco install git -y
choco install openjdk17 -y
choco install azure-data-studio -y
choco install docker-desktop -y